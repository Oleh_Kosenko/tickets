import { tickets } from './reducers/index'
import { init } from './reducers/index'
import generatePlaces from './generatePlaces'
import reduxStore from './store'

const rowsCount = Math.floor(Math.random() * 100)
const seatsCount = Math.floor(Math.random() * 200)
const roomsCount = 3

describe('tickets', () => {
    const store = reduxStore.getState()
    const places = generatePlaces(rowsCount, seatsCount)
    it('initial store data', () => {
        const state = tickets(init, {
            type: 'ALL_DATA'
        });
        expect(JSON.stringify(init)).toBe(JSON.stringify(state));
    });
    it('book ticket', () => {
        const currentNumber = Math.floor(Math.random() * roomsCount)
        places.rows.forEach((row, rowIndex) => {
            row.seats.forEach((seat, seatIndex) => {
                const newState = tickets(init, {
                    type: 'TICKET',
                    data: {
                        currentNumber,
                        booked: [
                            {
                                id: Math.random(),
                                row: rowIndex,
                                seat: seatIndex,
                                price: 80
                            }
                        ]
                    }
                });
                expect(JSON.stringify(store.tickets.data[currentNumber].booked)).toBe(JSON.stringify(newState.data[currentNumber].booked));
            });
        });
    });
});
