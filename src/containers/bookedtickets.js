import { connect } from 'react-redux';
import BookedTickets from '../Components/BookedTickets';


const mapStateToProps = state => {
  const { tickets } = state;
  const { data, currentNumber } = tickets;
  const { booked } = data[currentNumber]
  return {
    tickets,
    data,
    currentNumber,
    booked,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const ConnectedBookedTicket = connect(mapStateToProps, mapDispatchToProps)(BookedTickets);

export default ConnectedBookedTicket;
