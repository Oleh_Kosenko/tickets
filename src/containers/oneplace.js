import { connect } from 'react-redux';
import OnePlace from '../Components/OnePlace_Comp';


const mapStateToProps = state => {
  const { tickets } = state;
  let { data, currentNumber } = tickets;
  const { booked } = data[currentNumber]
  return{
    data,
    currentNumber,
    booked,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const ConnectedOnePlace = connect(mapStateToProps, mapDispatchToProps)(OnePlace);

export default ConnectedOnePlace;
