function generatePlaces(rowsCount, seatsCount) {
    const places = {
        rows: []
    }
    const averageSeatsInRow = Math.floor(seatsCount / (rowsCount - 1));
    const averageRows = Math.floor((rowsCount - 1) / 2);
    const lastRowSeatsCount = seatsCount % (rowsCount - 1);

    const rowSeatsCounts = new Array(rowsCount);
    let i = 0;
    for (let n = averageRows; n >= 0; n--) {
        rowSeatsCounts[n] = averageSeatsInRow - i * 2;
        if (n % 2 === 0) i++;
    }
    i = 0;
    for (let n = averageRows; n < rowsCount - 1; n++) {
        rowSeatsCounts[n] = averageSeatsInRow + i * 2;
        if (n % 2 !== 0) i++;
    }

    if (rowsCount < 6) {
        throw new Error("Неверное количество рядов. Должно быть не меньше 6-ти");
        // return {};
    }

    let newId = 1;

    for (let i = 0; i < rowsCount - 1; i++) {
        let row = {
            seats: []
        };
        for (let k = 1; k <= rowSeatsCounts[i]; k++) {
            let seat = {
                id: newId++,
                placeNumber: k,
                free: true,
            }
            row.seats.push(seat);
        }
        if (i < 4) {
            row.priceType = "cheap";
        }
        if (i > 3 && i < rowsCount - 1) {
            row.priceType = "expensive";
        }
        places.rows.push(row);
    }
    let row = {
        seats: []
    };
    for (let k = 1; k <= lastRowSeatsCount; k++) {
        let seat = {
            id: newId++,
            placeNumber: k,
            free: true,
        }
        row.seats.push(seat);
    }
    row.priceType = "vip";
    places.rows.push(row);

    return places;
}

export default generatePlaces;
