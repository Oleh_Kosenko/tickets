import React from 'react'
import {Link} from 'react-router-dom';

class OneLink extends React.Component {
    render = () => {
        let {name, link} = this.props;
        return (
            <div>
                <Link to={link}>{name}</Link>
            </div>
        )
    }
}

export default OneLink;