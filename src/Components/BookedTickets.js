import React from 'react';
import BookedTicket from './../containers/removeTicket';

class BookedTickets extends React.Component {
    buyTickets = () => {
        let { data, currentNumber, tickets } = this.props;
        data = data[currentNumber].booked;
        let sum = 0;
        data.forEach(ticket => {
            sum += Number(ticket.price);
        })

        localStorage.setItem("tickets", JSON.stringify(tickets))

        window.location.assign("/");

        alert(`Вы купили билеты на сумму: ${sum} грн.`);
    }

    render() {
        let { booked } = this.props;

        return (
            <div>
            <button onClick={this.buyTickets} hidden={(booked.length === 0)} style={{color: "darkblue"}}>Купить</button>
            <ul>
                {
                    booked.map(item => (
                        <BookedTicket
                            booked={booked}
                            key={item.id}
                            id={item.id}
                            row={item.row}
                            seat={item.seat}
                            price={item.price}
                        />
                    ))
                }
            </ul>
            </div>
        )
    }
}

export default BookedTickets;
