<h4>This wonderful app for on-line tickets booking
You can easily choose tickets and buy them
All data about tickets you have bought are stored in localStorage of your browser</h4>

## To install run npm install

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
For testing, the app uses Jest library

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm run start_server`

Starting web-server using Node JS Express framework<br>
Open [http://localhost:3333](http://localhost:3333) to view it in the browser.

## Afterwords
Have fun with JS, it's really great and awesome programming language!
